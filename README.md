# ggsrv

A simple server to handle `go get` requests to a vanity, or custom, domain name.

## Installation

[As a systemd service on a Debian system](systemd.md)

[As an appengine service on Google Cloud Platform](appengine.md)