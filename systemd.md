These instructions will get you set up running ggsrv as a service with systemd. I used Debian 8, most systemd based linux distributions should be similar.

get and install the latest version `go get -u rjl.li/ggsrv`

## Note:
The source files are set up to work with appengine by default, to use as a standalone app you need to replace `appengine.Main()` with `log.Fatal(http.ListenAndServe(s.Port, nil))` in `main.go`.

## Usage

`ggsrv` will run the server _quietly_, that is only logging fatal errors.

`ggsrv -v` will run the server _verbosely_, logging requests to standard logger.

It can be run in the background `ggsrv &`, but it is recommended to be run as a service as described in the installation instructions.

### Executable

Copy the binary file:
`sudo cp /path/to/ggsrv /usr/local/bin/ggsrv`

The binary should be owned by root:
`sudo chown root:root /usr/local/bin/ggsrv`

Make sure the binary is executable:
`sudo chmod a+x /usr/local/bin/ggsrv`

Depending on the port you want to bind to you may need to allow ggsrv access:
`setcap 'cap_net_bind_service=+ep' /usr/local/bin/ggsrv`

### Configuration

ggsrv reads from a JSON file called `ggsrv.config.json`, at the root of the present working directory.

The file can be located anywhere you like, I am using caddy so I keep this config file in `/home/caddy` with other server configuration stuff.

Optionally you can specify another location with the flag `-conf`, i.e. `ggsrv -conf /home/myuser/ggsrv.conf`. It must only be readable and valid json as described below.

simple `ggsrv.config.json`
```
{
  "RepoRoot": "https://gitlab.com/robjloranger",
}
```
possible settings and their defaults
```
{
  "RepoRoot": no-default, // _required_
  "VCS": "git", // any supported vcs below
  "Port": "2121", // any available port
}
```
`VCS` is the file suffix for your version control system.
```
Bazaar      bzr
Git         git
Mercurial   hg
Subversion  svn
```

Remember where you save this as we need to reference it from our service configuration below.

### Systemd Service

The systemd service should be in `/etc/systemd/system/ggsrv.service`

example `ggsrv.service`
```
[Unit]
Description=ggsrv - Go Import Handler
Documentation=https://rjl.li/ggsrv/README.md
After=network-online.target

[Service]
Type=simple
User=caddy
Group=caddy
ExecStart=/usr/local/bin/ggsrv
WorkingDirectory=/home/caddy
ExecReload=/bin/kill -USR1 $MAINPID
LimitNOFILE=16384
Restart=on-failure
StartLimitInterval=600

[Install]
WantedBy=multi-user.target
```

`WorkingDirectory` should point to where you chose to save the `ggsrv.config.json` from above, __unless__ you used the `-conf` flag.

You'll also notice that I am sharing a user with my caddy server for simplicity.

Enable the new service: `sudo systemctl enable ggsrv.service`

Start it up:            `sudo systemctl start ggsrv`

Check it's status:      `systemctl status ggsrv`, note you don't need sudo here.

If you make changes to this service file you must reload the daemons before starting again.
```
sudo systemctl daemon-reload
sudo systemctl start ggsrv
```

### Server Set Up
TODO: must be https for go get to work

2017 rob j loranger [hello@robloranger.ca](mailto://hello@robloranger.ca)