package main

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestHandler(t *testing.T) {
	cases := []struct {
		in, out string
		status  int
	}{
		{
			"http://localhost/test?go-get=1",
			`<meta name="go-import" content="localhost/test git https://gitlab.com/robjloranger/test.git">`,
			200,
		},
		{
			"http://localhost/test",
			"<a href=\"https://gitlab.com/robjloranger/test\">Temporary Redirect</a>.",
			307,
		},
	}
	for _, c := range cases {
		req, err := http.NewRequest(
			http.MethodGet,
			c.in,
			nil,
		)
		if err != nil {
			t.Fatalf("could not create request: %v\n", err)
		}
		rec := httptest.NewRecorder()
		handler(rec, req)
		if rec.Code != c.status {
			t.Errorf("expecting status %d; got %d\n", c.status, rec.Code)
		}
		if !strings.Contains(rec.Body.String(), c.out) {
			t.Errorf("for %s, unexpexted body in response: %s\n", c.in, rec.Body.String())
		}
	}
}

func BenchmarkHandler(b *testing.B) {
	for i := 0; i < b.N; i++ {
		req, err := http.NewRequest(
			http.MethodGet,
			"http://localhost/test?go-get=1",
			nil,
		)
		if err != nil {
			b.Fatalf("could not create request: %v\n", err)
		}
		rec := httptest.NewRecorder()
		handler(rec, req)
		if rec.Code != http.StatusOK {
			b.Errorf("expecting status 200; got %d\n", rec.Code)
		}
		if !strings.Contains(rec.Body.String(), `<meta name="go-import" content="localhost/test git https://gitlab.com/robjloranger/test.git">`) {
			b.Errorf("unexpexted body in response: %q\n", rec.Body.String())
		}
	}
}
