These instructions explain how to use another tool, called [gacertsbot](https://github.com/hatstand/gacertsbot/), to set up and automaticaly renew ssl certificates from [LetsEncrypt](https://letsencrypt.org). This application requires https for go get to work.

## Deploy Server

Get the latest version with `go get -u rjl.li/ggsrv`.

Navigate to `$GOPATH/src/rjl.li/ggsrv`. TODO: create/set project

`gcloud app deploy`

### Domain Set up
TODO:

## Deploy SSL Module

I'll leave the instructions to the author, they can be viewed [here](https://github.com/hatstand/gacertsbot/). Get a head start with `go get -u github.com/hatstand/gacertsbot`.