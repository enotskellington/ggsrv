// ggsrv is a handler for go import requests.
//
// If the url has the go-get query and is equal to 1, go-get=1.
// The server responds with a meta tag named 'go-import' which
// contains the import url, the repo type and the repo url.

package main

import (
	"encoding/json"
	"flag"
	"io"
	"io/ioutil"
	"log"
	"net/http"

	"google.golang.org/appengine"
)

const version string = "v0.10"

var configFile = flag.String("conf", "ggsrv.config.json", "server settings in json format")
var verbose = flag.Bool("v", false, "verbose output")

// initialize s with default settings
var s = struct {
	RepoRoot, VCS, Port string
}{
	VCS:  "git",
	Port: "8080",
}

// parse flags, read config and unmarshal the data, or fail
func init() {
	flag.Parse()
	log.Println("ggsrv " + version + " starting")
	f, err := ioutil.ReadFile(*configFile)
	if err != nil {
		log.Fatalf("failed opening config: %v\n", err)
	}
	// load any new config into s
	err = json.Unmarshal(f, &s)
	if err != nil {
		log.Fatalf("failed to read json from config: %v\n", err)
	}
}

// handler decides to redirect or return formatted html
func handler(w http.ResponseWriter, r *http.Request) {
	host := r.Host
	uri := r.URL.Path[1:]
	vanity := host + "/" + uri
	url := s.RepoRoot + "/" + uri
	if r.URL.RawQuery == "go-get=1" { // check for go-get requests
		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		if *verbose {
			log.Printf("getting: %s/%s over %v\n", host, uri, r.Proto)
		}
		_, err := io.WriteString(w, `<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
		<meta name="go-import" content="`+vanity+` `+s.VCS+` `+url+`.`+s.VCS+`">
  </head>
  <body>
    <h1>`+vanity+`</h1>
		<p>If your looking for the project's code repository, try visiting this link <a href="`+url+`">`+url+`</a>.</p>
		<hr/>
    <p>Vanity imports served by <a href="https://rjl.li/ggsrv">ggsrv</a>.</p>
  </body>
</html>
`)
		if err != nil {
			http.Error(w, "failed to write response", http.StatusInternalServerError)
			return
		}
	} else { // otherwise just redirect
		if *verbose {
			log.Printf("redirect 307: %s\n", url)
		}
		http.Redirect(w, r, url, http.StatusTemporaryRedirect)
	}
}

func robotHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "robots.txt")
}

func main() {
	http.HandleFunc("/", handler)
	http.HandleFunc("/robots.txt", robotHandler)
	http.Handle("/favicon.ico", http.NotFoundHandler())
	appengine.Main()
}
